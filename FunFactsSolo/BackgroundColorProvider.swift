//
//  BackgroundColorProvider.swift
//  FunFactsSolo
//
//  Created by kenya on 25/02/2019.
//  Copyright © 2019 Kenya Geronimo. All rights reserved.
//

import UIKit
import GameKit

struct BackgroundColorProvider {
    let colors = [
        UIColor(red: 215/255.0, green: 221/255.0, blue: 192/255.0, alpha: 1.0),
        UIColor(red: 217/255.0, green: 215/255.0, blue: 223/255.0, alpha: 1.0),
        UIColor(red: 239/255.0, green: 234/255.0, blue: 187/255.0, alpha: 1.0),
        UIColor(red: 251/255.0, green: 211/255.0, blue: 203/255.0, alpha: 1.0),
        UIColor(red: 220/255.0, green: 212/255.0, blue: 225/255.0, alpha: 1.0),
        UIColor(red: 174/255.0, green: 159/255.0, blue: 186/255.0, alpha: 1.0)]
    
    func randomColor() -> UIColor {
        let randomNumber = GKRandomSource.sharedRandom().nextInt(upperBound: colors.count)
        return colors[randomNumber]
    }

}
